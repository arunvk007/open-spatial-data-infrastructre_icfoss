# Install Python 3 package manager (pip3) and its dependencies
sudo apt install python3-pip

# Install virtualenv using pip3
pip3 install virtualenv

# Check the version of pip3
pip3 --version

# Install virtualenvwrapper using pip3
pip3 install virtualenvwrapper

# Set the path to the Python interpreter for virtualenvwrapper
export VIRTUALENVWRAPPER_PYTHON='/usr/bin/python3'

# Source the virtualenvwrapper script
source /usr/local/bin/virtualenvwrapper.sh
# Alternatively, use the following if the above line does not work:
# source ~/.local/bin/virtualenvwrapper.sh

# Create a virtual environment for the project using Python 3.9
mkvirtualenv --python=/usr/bin/python3 geonode_env


